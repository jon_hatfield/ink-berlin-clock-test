package com.inkglobal.techtest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.inkglobal.techtest.services.BerlinClockFactory;
import com.inkglobal.techtest.services.BerlinClockService;
import com.inkglobal.techtest.services.ClockFactory;
import com.inkglobal.techtest.services.ClockService;
import com.inkglobal.techtest.util.TimeParserUtils;

@Configuration
public class BerlinClockConfig {

	@Bean
	public ClockService standardBerlinClock(){
		return new BerlinClockService();
	}
	
	@Bean
	public TimeParserUtils timeParser(){
		return new TimeParserUtils();
	}
	
	@Bean
	public ClockFactory clockFactory(){
		return new BerlinClockFactory();
	}
}