package com.inkglobal.techtest.pojo;

import java.text.ParseException;
import java.util.List;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.inkglobal.techtest.config.BerlinClockConfig;
import com.inkglobal.techtest.services.BerlinClockService;
import com.inkglobal.techtest.services.ClockService;
import com.inkglobal.techtest.util.BerlinClockConstants;
import com.inkglobal.techtest.vo.TimeStamp;

public class BerlinClock implements Clock {
	private final ClockLight secondsLight;
	private final List<ClockLight> fiveHourRow;
	private final List<ClockLight> oneHourRow;
	private final List<ClockLight> fiveMinuteRow;
	private final List<ClockLight> oneMinuteRow;
	
	public BerlinClock(ClockLight secondsLight, List<ClockLight> fiveHourRow, List<ClockLight> oneHourRow,
			List<ClockLight> fiveMinuteRow, List<ClockLight> oneMinuteRow){
		this.secondsLight = secondsLight;
		this.fiveHourRow = fiveHourRow;
		this.oneHourRow = oneHourRow;
		this.fiveMinuteRow = fiveMinuteRow;
		this.oneMinuteRow = oneMinuteRow;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(secondsLight).append(" ");
		appendRow(sb, fiveHourRow);
		sb.append(" ");
		appendRow(sb, oneHourRow);
		sb.append(" ");
		appendRow(sb, fiveMinuteRow);
		sb.append(" ");
		appendRow(sb, oneMinuteRow);
		return sb.toString();
	}
	
	private void appendRow(StringBuilder sb, List<ClockLight> row){
		for(ClockLight light : row){
			sb.append(light);
		}
	}
	
	public void setTime(TimeStamp time){
		boolean secondsOn = (time.getSeconds() % BerlinClockConstants.EVEN_NUMBER_MODULUS) == 0;
		int fiveTimeUnits = BerlinClockConstants.FIVE_TIME_UNITS;
		int fiveHourBlocksOn = time.getHours() / fiveTimeUnits;
		int oneHourBlocksOn = time.getHours() % fiveTimeUnits;
		int fiveMinuteBlocksOn = time.getMinutes() / fiveTimeUnits;
		int oneMinuteBlocksOn = time.getMinutes() % fiveTimeUnits;
		
		if(secondsOn){
			secondsLight.setOn(true);
		}
		setRowOfLights(fiveHourRow, fiveHourBlocksOn);
		setRowOfLights(oneHourRow, oneHourBlocksOn);
		setRowOfLights(fiveMinuteRow, fiveMinuteBlocksOn);
		setRowOfLights(oneMinuteRow, oneMinuteBlocksOn);
	}
	
	private void setRowOfLights(List<ClockLight> row, int lightsOn){
		for(int i = 0; i < lightsOn; i++){
			row.get(i).setOn(true);
		}
	}
	
	public static void main(String[] args) throws ParseException{
		ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(BerlinClockConfig.class);
		ClockService standardBerlinClock = context.getBean(BerlinClockService.class);
		context.close();
		String exampleInput = "20:54:55";
		System.out.println("Example Berlin Clock output for input "+exampleInput+":");
		System.out.println(standardBerlinClock.translateTime(exampleInput));
	}
}