package com.inkglobal.techtest.pojo;

import com.inkglobal.techtest.vo.TimeStamp;

public interface Clock {
	public void setTime(TimeStamp time);
}