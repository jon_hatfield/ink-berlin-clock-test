package com.inkglobal.techtest.pojo;

import com.inkglobal.techtest.util.ClockLightColour;
import com.inkglobal.techtest.util.TimeUnit;

public class ClockLight {
	private boolean on;
	private final TimeUnit timeUnit;
	private ClockLightColour colour;
	
	public ClockLight(boolean on, TimeUnit timeUnit, ClockLightColour colour) {
		this.on = on;
		this.timeUnit = timeUnit;
		this.colour = colour;
	}
	
	@Override
	public String toString(){
		if(on){
			return colour.getLabel();
		}
		return ClockLightColour.NONE.getLabel();
	}

	public boolean isOn() {
		return on;
	}
	
	public void setOn(boolean on){
		this.on = on;
	}

	public TimeUnit getTimeUnit() {
		return timeUnit;
	}

	public ClockLightColour getColour() {
		return colour;
	}

	public void setColour(ClockLightColour colour) {
		this.colour = colour;
	}
}