package com.inkglobal.techtest.services;

import java.util.ArrayList;
import java.util.List;

import com.inkglobal.techtest.pojo.BerlinClock;
import com.inkglobal.techtest.pojo.Clock;
import com.inkglobal.techtest.pojo.ClockLight;
import com.inkglobal.techtest.util.BerlinClockConstants;
import com.inkglobal.techtest.util.ClockLightColour;
import com.inkglobal.techtest.util.TimeUnit;

public class BerlinClockFactory implements ClockFactory {
	
	public Clock getClock(){
		ClockLight secondsLight = new ClockLight(false, TimeUnit.SECONDS, ClockLightColour.YELLOW);
		List<ClockLight> fiveHourRow = new ArrayList<ClockLight>();
		initializeLights(fiveHourRow, BerlinClockConstants.FIVE_HOUR_SECTIONS, TimeUnit.HOURS, ClockLightColour.RED);
		List<ClockLight> oneHourRow = new ArrayList<ClockLight>();
		initializeLights(oneHourRow, BerlinClockConstants.ONE_HOUR_SECTIONS, TimeUnit.HOURS, ClockLightColour.RED);
		
		List<ClockLight> fiveMinuteRow = new ArrayList<ClockLight>();
		fiveMinuteRow = new ArrayList<ClockLight>();
		initializeLights(fiveMinuteRow, BerlinClockConstants.FIVE_MINUTE_SECTIONS, TimeUnit.MINUTES, ClockLightColour.YELLOW);
		for(int i = 0; i < fiveMinuteRow.size(); i++){
			if((i + 1) % BerlinClockConstants.QUARTER_HOUR_MODULUS == 0){
				fiveMinuteRow.get(i).setColour(ClockLightColour.RED);
			}
		}
		
		List<ClockLight> oneMinuteRow = new ArrayList<ClockLight>();
		initializeLights(oneMinuteRow, BerlinClockConstants.ONE_MINUTE_SECTIONS, TimeUnit.MINUTES, ClockLightColour.YELLOW);
		return new BerlinClock(secondsLight, fiveHourRow, oneHourRow, fiveMinuteRow, oneMinuteRow);
	}
	
	private void initializeLights(List<ClockLight> row, int sectionCount, TimeUnit timeUnit, ClockLightColour colour) {
		for(int i = 0; i < sectionCount; i++){
			row.add(new ClockLight(false, timeUnit, colour));
		}
	}
}