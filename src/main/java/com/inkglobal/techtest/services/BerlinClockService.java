package com.inkglobal.techtest.services;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;

import com.inkglobal.techtest.pojo.BerlinClock;
import com.inkglobal.techtest.pojo.Clock;
import com.inkglobal.techtest.util.TimeParserUtils;
import com.inkglobal.techtest.vo.TimeStamp;

public class BerlinClockService implements ClockService {
	
	@Autowired
	private TimeParserUtils timeParser;
	
	@Autowired
	private ClockFactory clockFactory;

	/**
	 * Converts hh:MM:ss String representation of time into a Berlin Clock String representation
	 */
	@Override
	public String translateTime(String timeString) throws ParseException {
		TimeStamp timeStamp = timeParser.injectableParseTimeString(timeString);
		Clock berlinClock = clockFactory.getClock();
		berlinClock.setTime(timeStamp);
		return berlinClock.toString();
	}
}