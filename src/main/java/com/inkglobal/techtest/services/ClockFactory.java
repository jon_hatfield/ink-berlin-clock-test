package com.inkglobal.techtest.services;

import com.inkglobal.techtest.pojo.Clock;

public interface ClockFactory {
	public Clock getClock();
}