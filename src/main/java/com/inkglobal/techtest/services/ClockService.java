package com.inkglobal.techtest.services;

import java.text.ParseException;

public interface ClockService {
	
	/**
	 * Converts hh:MM:ss String representation of time into another String representation
	 * @param time
	 * @return
	 * @throws ParseException
	 */
	public String translateTime(String time) throws ParseException;
}