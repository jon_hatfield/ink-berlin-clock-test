package com.inkglobal.techtest.util;

public final class BerlinClockConstants {
	public static final String TIME_INPUT_FORMAT = "HH:mm:ss";
	
	public static final int FIVE_HOUR_SECTIONS = 4;
	public static final int ONE_HOUR_SECTIONS = 4;
	public static final int FIVE_MINUTE_SECTIONS = 11;
	public static final int ONE_MINUTE_SECTIONS = 4;
	
	public static final int FIVE_TIME_UNITS = 5;
	public static final int EVEN_NUMBER_MODULUS = 2;
	
	public static final int QUARTER_HOUR_MODULUS = 3;
	
	public static final String MIDNIGHT_ALIAS_ONE = "00";
	public static final String MIDNIGHT_ALIAS_TWO = "24";
}