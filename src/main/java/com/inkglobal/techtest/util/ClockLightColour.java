package com.inkglobal.techtest.util;

public enum ClockLightColour {
	YELLOW("Y"),
	RED("R"),
	NONE("O");
	
	private String label;
	
	private ClockLightColour(String label){
		this.label = label;
	}
	
	public String getLabel(){
		return label;
	}
}