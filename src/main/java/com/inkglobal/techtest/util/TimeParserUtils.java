package com.inkglobal.techtest.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.inkglobal.techtest.vo.TimeStamp;

public final class TimeParserUtils {
	
	public static TimeStamp parseTimeString(String input) throws ParseException{
		SimpleDateFormat formatter = new SimpleDateFormat(BerlinClockConstants.TIME_INPUT_FORMAT);
        Date time = formatter.parse(input);
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(time);
        
        int hours;
        if(input.startsWith(BerlinClockConstants.MIDNIGHT_ALIAS_ONE)){
        	hours = Integer.parseInt(BerlinClockConstants.MIDNIGHT_ALIAS_ONE);
        }
        else if(input.startsWith(BerlinClockConstants.MIDNIGHT_ALIAS_TWO)){
        	hours = Integer.parseInt(BerlinClockConstants.MIDNIGHT_ALIAS_TWO);
        }
        else{
        	hours = cal.get(Calendar.HOUR_OF_DAY);
        }
        return new TimeStamp(hours, cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
	}
	
	public TimeStamp injectableParseTimeString(String input) throws ParseException{
		return parseTimeString(input);
	}
}