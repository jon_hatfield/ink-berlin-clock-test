package com.inkglobal.techtest.util;

public enum TimeUnit {
	HOURS,
	MINUTES,
	SECONDS;
}