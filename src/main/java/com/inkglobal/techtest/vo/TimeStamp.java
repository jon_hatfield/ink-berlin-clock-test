package com.inkglobal.techtest.vo;

public class TimeStamp {
	private final int hours;
	private final int minutes;
	private final int seconds;
	
	public TimeStamp(int hours, int minutes, int seconds) {
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}

	public int getHours() {
		return hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public int getSeconds() {
		return seconds;
	}	
}