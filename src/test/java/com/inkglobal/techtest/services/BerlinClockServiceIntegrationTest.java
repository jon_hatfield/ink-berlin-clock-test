package com.inkglobal.techtest.services;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.inkglobal.techtest.pojo.BerlinClock;
import com.inkglobal.techtest.util.TimeParserUtils;

/**
 * Integration test to test the full path from input to output.
 * 
 * Unit tests would be required too in a full scale system with the dependencies mocked.
 * 
 * @author Jon
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=BerlinClockTestConfig.class, loader=AnnotationConfigContextLoader.class)
public class BerlinClockServiceIntegrationTest {
	
	@Autowired
	private ClockService instance;
	
	@Test
	public void testTranslateTime() throws ParseException{
		String time = "00:00:00";
		String result = instance.translateTime(time);
		assertEquals("Y OOOO OOOO OOOOOOOOOOO OOOO", result);
		
		time = "13:17:01";
		result = instance.translateTime(time);
		assertEquals("O RROO RRRO YYROOOOOOOO YYOO", result);
		
		time = "23:59:59";
		result = instance.translateTime(time);
		assertEquals("O RRRR RRRO YYRYYRYYRYY YYYY", result);
		
		time = "24:00:00";
		result = instance.translateTime(time);
		assertEquals("Y RRRR RRRR OOOOOOOOOOO OOOO", result);
	}
}