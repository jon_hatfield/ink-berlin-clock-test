package com.inkglobal.techtest.services;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.inkglobal.techtest.util.TimeParserUtils;

@Configuration
public class BerlinClockTestConfig {

	@Bean
	public ClockService standardBerlinClock(){
		return new BerlinClockService();
	}
	
	@Bean
	public TimeParserUtils timeParser(){
		return new TimeParserUtils();
	}
	
	@Bean
	public ClockFactory clockFactory(){
		return new BerlinClockFactory();
	}
}